<?php return [
    'tld'         => 'test',

    // used for pagination - recommended max 20
    'max_results' => 20,
];