<?php

namespace App\Http\Controllers;

use App\Entities\Folder;
use App\Http\Requests\FolderRequest;

class FoldersController extends Controller
{

    public function index()
    {
        $folders = Folder::orderBy('name')->get();
        return view('pages.folders.index', compact('folders'));
    }


    public function edit(Folder $folder)
    {
        return view('pages.folders.edit', compact('folder'));
    }


    public function update(FolderRequest $request, Folder $folder)
    {
        $folder->update($request->all());
        return redirect(route('folders.index'));
    }

    public function create()
    {
        return view('pages.folders.create');
    }


    public function store(FolderRequest $request)
    {
        Folder::create($request->all());
        return redirect(route('folders.index'));
    }


    public function delete(Folder $folder)
    {
        $folder->delete();
        return redirect(route('folders.index'));
    }
}
