<?php

namespace App\Http\Controllers;

use App\Entities\Setting;
use App\Http\Requests\SettingRequest;
use Illuminate\Http\Request;

class SettingsController extends Controller
{


    public function index()
    {
        $settings = Setting::orderBy('key')->get();
        return view('pages.settings.index', compact('settings'));
    }


    public function edit(Setting $setting)
    {
        return view('pages.settings.edit', compact('setting'));
    }


    public function update(SettingRequest $request, Setting $setting)
    {
        $setting->update($request->all());
        return redirect(route('settings.index'));
    }

    public function create()
    {
        return view('pages.settings.create');
    }


    public function store(SettingRequest $request)
    {
        Setting::create($request->all());
        return redirect(route('settings.index'));
    }


    public function delete(Setting $setting)
    {
        $setting->delete();
        return redirect(route('settings.index'));
    }

}
