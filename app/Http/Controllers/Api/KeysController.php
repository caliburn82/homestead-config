<?php

namespace App\Http\Controllers\Api;

use App\Entities\Key;
use App\Http\Requests\KeyRequest;

class KeysController extends Api
{

    public function get(Key $key = null)
    {
        return $this->_get($key ?: Key::class, ['id', 'location', 'active']);
    }


    public function put(KeyRequest $request, Key $key = null)
    {
        return $this->_put($request, $key ?: new Key);
    }

    public function patch(KeyRequest $request, Key $key)
    {
        return $this->_patch($request, $key);
    }

    public function delete(Key $key)
    {
        return $this->_delete($key);
    }

}
