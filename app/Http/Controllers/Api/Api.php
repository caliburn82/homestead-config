<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;


abstract class Api extends Controller
{


    /**
     * @param Model|string $model
     * @param array $fields
     * @return Model|Collection
     */
    protected function _get($model = null, $paginatedFields = ['*'], $hiddenFields = [])
    {

        $request = request();

        $fields = collect(explode(',', $request->get('fields')))->filter();

        if (is_object($model)) {
            if (!$fields->isEmpty()) {
                $known = $model->getFillable();
                $model->makeHidden($known)->makeVisible($fields->all());
            }

            return $model->makeHidden($hiddenFields);
        }


        // todo: filter fields and paginate


        /** @var Model $model */
        $model = new $model;
        $query = $model::getQuery()->select($fields);

        $request = new Collection(request()->only($model->getFillable()));
        $request->isEmpty() || $query->where($request->all());

        return $this->_paginate($query);
    }


    protected function _paginate(Builder $query)
    {

    }


    protected function _put(Request $request, Model $model)
    {
        $blank = array_fill_keys($model->getFillable(), null);
        $model->fill($request->all() + $blank)->save();

        return new Collection(['success' => true]);
    }


    protected function _patch(Request $request, Model $model)
    {
        $model->update($request->all());

        return new Collection(['success' => true]);
    }

    /**
     * @param Model $model
     * @return string
     * @throws \Exception
     */
    protected function _delete(Model $model)
    {
        $model->delete();

        return new Collection(['success' => true]);
    }


}