<?php

namespace App\Http\Controllers\Api;

use App\Entities\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function get(Setting $setting = null)
    {
        return $setting ?: Setting::all(['id', 'key', 'value']);
    }


    public function put(Request $request, Setting $setting = null)
    {

        $setting = $setting ?: new Setting;

        $blank   = array_fill_keys($setting->getFillable(), null);
        $success = $setting->fill($request->all() + $blank)->save();

        return json_encode($success);

    }

    public function patch(Request $request, Setting $setting)
    {
        $success = $setting->update($request->all());
        return json_encode($success);
    }

    /**
     * @param Setting $setting
     * @return string
     * @throws \Exception
     */
    public function delete(Setting $setting)
    {
        $success = $setting->delete();
        return json_encode($success);

    }

}
