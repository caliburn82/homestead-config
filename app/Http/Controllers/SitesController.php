<?php

namespace App\Http\Controllers;

use App\Entities\Folder;
use App\Entities\Group;
use App\Entities\Site;
use App\Http\Requests\SiteRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SitesController extends Controller
{

    public function index()
    {
        $sites = Site::orderBy('url')->with('group')->get();

        $active = $sites->where('active', true)->groupBy('group.name')
                        ->sortBy(function ($value, $key) {
                            return $key;
                        });

        $inactive = $sites->where('active', false)->groupBy('group.name')
                          ->sortBy(function ($value, $key) {
                              return $key;
                          });

        return view('pages.sites.index', compact('active', 'inactive'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function updateAll(Request $request)
    {

        \DB::transaction(function () use ($request) {
            Site::where('active', true)->update(['active' => false]);
            Site::whereIn('id', $request->active)->update(['active' => true]);
        });

        return redirect(route('sites.index'));
    }


    public function edit(Site $site)
    {
        $groups  = Group::orderBy('name')->pluck('name', 'id');
        $folders = Folder::pluck('name', 'id');
        return view('pages.sites.edit', compact('site', 'groups', 'folders'));
    }


    public function update(SiteRequest $request, Site $site)
    {
        $site->update($request->all());

        if (strtolower($request->action) == 'duplicate') {
            $defaults = ['active' => 1, 'inherit_tld' => 1, 'schedule' => 0];
            return redirect(route('sites.create'))->withInput($defaults + $request->except('action'));
        }

        return redirect(route('sites.index'));
    }

    public function create()
    {
        session()->reflash();

        $groups  = Group::orderBy('name')->pluck('name', 'id');
        $folders = Folder::pluck('name', 'id');
        return view('pages.sites.create', compact('groups', 'folders'));
    }


    public function store(SiteRequest $request)
    {
        Site::create($request->all());
        return redirect(route('sites.index'));
    }


    public function delete(Site $site)
    {
        $site->delete();
        return redirect(route('sites.index'));
    }
}
