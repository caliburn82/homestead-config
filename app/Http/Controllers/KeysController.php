<?php

namespace App\Http\Controllers;

use App\Entities\Key;
use App\Http\Requests\KeyRequest;

class KeysController extends Controller
{

    public function index()
    {
        $keys = Key::all();
        return view('pages.keys.index', compact('keys'));
    }


    public function edit(Key $key)
    {
        return view('pages.keys.edit', compact('key'));
    }


    public function update(KeyRequest $request, Key $key)
    {
        $key->update($request->all());
        return redirect(route('keys.index'));
    }

    public function create()
    {
        return view('pages.keys.create');
    }


    public function store(KeyRequest $request)
    {
        Key::create($request->all());
        return redirect(route('keys.index'));
    }


    public function delete(Key $key)
    {
        $key->delete();
        return redirect(route('keys.index'));
    }
}
