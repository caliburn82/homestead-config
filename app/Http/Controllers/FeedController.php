<?php

namespace App\Http\Controllers;

use App\Entities\Site;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Roumen\Feed\Feed;

class FeedController extends Controller
{

    public function active()
    {
        $sites = Site::where('active', true)
                     ->with([
                         'group' => function (Relation $query) {
                             $query->select('id', 'tld', 'name');
                         },
                     ])->get()
                     ->sortBy(function (Site $site) {
                         return ($site->group->name ?? '#Blank') . " - $site->url";
                     });

        $feed = new Feed();

        $feed->title       = 'Active Dev Sites';
        $feed->description = 'Currently active sites for local development';
        $feed->link        = route('home');
        $feed->pubdate     = Site::max('updated_at');

        $tld = config('cm.tld');
        $sites->each(function (Site $site) use ($feed, $tld) {
            $url = [$site->url];
            if ($site->inherit_tld) {
                $url[] = $site->group->tld ?? '';
                $url[] = $tld;
            }
            $url = collect($url)->filter()->implode('.');


            $siteName = str_replace('-', ' ', $site->url);
            $siteName = ucwords($siteName);

            $feed->add($url,
                'Jon',
                "http://$url",
                $site->updated_at,
                ($site->group->name ?? 'No group') . ": $siteName"
            );
        });


        return $feed->render('atom');
    }

}
