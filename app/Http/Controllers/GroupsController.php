<?php

namespace App\Http\Controllers;

use App\Entities\Group;
use App\Http\Requests\GroupRequest;

class GroupsController extends Controller
{

    public function index()
    {
        $groups = Group::orderBy('name')->get();
        return view('pages.groups.index', compact('groups'));
    }


    public function edit(Group $group)
    {
        return view('pages.groups.edit', compact('group'));
    }


    public function update(GroupRequest $request, Group $group)
    {
        $group->update($request->all());
        return redirect(route('groups.index'));
    }

    public function create()
    {
        return view('pages.groups.create');
    }


    public function store(GroupRequest $request)
    {
        Group::create($request->all());
        return redirect(route('groups.index'));
    }


    public function delete(Group $group)
    {
        $group->delete();
        return redirect(route('groups.index'));
    }
}
