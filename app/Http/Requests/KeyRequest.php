<?php

namespace App\Http\Requests;

class KeyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $uniqueLocation = $this->key ? ',' . $this->key->id : '';

        $rules = [
            'active'   => ['boolean'],
            'location' => ['string', 'unique:keys,location' . $uniqueLocation,]
        ];

        if ($this->method() != 'PATCH') {
            $rules['location'][] = 'required';
        }

        return $rules;
    }

}
