<?php

namespace App\Http\Requests;


class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $uniqueKey = $this->setting ? ',' . $this->setting->id : '';

        $rules = [
            'key' => ['unique:settings,key' . $uniqueKey],
        ];

        if ($this->method() != 'PATCH') {
            $rules['key'][]   = 'required';
            $rules['value'][] = 'required';
        }

        return $rules;
    }
}
