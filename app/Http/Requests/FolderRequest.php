<?php

namespace App\Http\Requests;

class FolderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $uniqueName = $this->folder ? ',' . $this->folder->id : '';

        $rules = [
            'name' => ['unique:folders,name' . $uniqueName,],
        ];

        if ($this->method() != 'PATCH') {
            $rules['name'][] = 'required';
            $rules['from'][] = 'required';
            $rules['to'][]   = 'required';
        }

        return $rules;
    }

}
