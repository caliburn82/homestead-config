<?php

namespace App\Http\Requests;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $uniqueName = $this->group ? ',' . $this->group->id : '';

        $rules = [
            'name' => ['unique:groups,name' . $uniqueName,],
            'tld'  => 'regex:/^[a-z0-9][a-z0-9.\-_]*[a-z0-9]$/i'
        ];

        if ($this->method() != 'PATCH') {
            $rules['name'][] = 'required';
        }

        return $rules;
    }


    public function messages()
    {
        return [
            'tld.regex' => 'invalid top level domain'
        ];
    }

}
