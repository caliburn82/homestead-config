<?php

namespace App\Http\Requests;

class SiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];
        if ($this->method() != 'PATCH') {
            $rules['folder_id'][] = 'required';
        }

        return $rules;
    }


}
