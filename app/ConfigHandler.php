<?php


namespace App;


use App\Entities\Folder;
use App\Entities\Key;
use App\Entities\Port;
use App\Entities\Setting;
use App\Entities\Site;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Symfony\Component\Yaml\Yaml;

class ConfigHandler
{

    public static function render()
    {
        $s = new static();

        $config = $s->settings();
        $config->put('keys', $s->keys());
        $config->put('folders', $s->folders());
        $config->put('sites', $s->sites());
        $config->put('databases', $s->databases());
        $config->put('ports', $s->ports());

        return Yaml::dump($config->toArray(), 3);

    }


    private function settings()
    {
        return Setting::pluck('value', 'key');
    }


    private function keys()
    {
        return Key::where('active', true)->pluck('location');
    }


    private function folders()
    {
        return Folder::select('from', 'to', 'type')
                     ->whereHas('sites', function (Builder $query) {
                         $query->where('active', true);
                     })->get()
                     ->map(function (Folder $folder) {
                         return collect([
                             'map'  => $folder->from,
                             'to'   => $folder->to,
                             'type' => $folder->type
                         ])->filter();
                     });
    }

    private function sites()
    {
        $tld = config('cm.tld');

        return Site::where('active', true)
                   ->with([
                       'group'  => function (Relation $query) {
                           $query->select('id', 'tld', 'name');
                       },
                       'folder' => function (Relation $query) {
                           $query->select('id', 'to');
                       },
                   ])->get()
                   ->sortBy(function (Site $site) {
                       return ($site->group->name ?? '#Blank') . $site->url;
                   })
                   ->map(function (Site $site) use ($tld) {
                       $url = new Collection([$site->url]);

                       if ($site->inherit_tld) {
                           $url = $url->merge([
                               $site->group->tld ?? null,
                               $tld
                           ])->filter();
                       }


                       return (new Collection([
                           'map'      => $url->implode('.'),
                           'to'       => $site->folder->to . '/' . $site->path,
                           'type'     => $site->type,
                           'php'      => $site->version,
                           'schedule' => $site->schedule,
                       ]))->filter();
                   })->values();

    }

    private function ports()
    {
        return Port::where('active', true)->get(['from', 'to', 'protocol'])
                   ->map(function (Port $port) {

                       return collect([
                           'send'     => $port->from,
                           'to'       => $port->to ?: $port->from,
                           'protocol' => $port->protocol
                       ])->filter();

                   });
    }

    private function databases()
    {
        return Site::where('active', true)->pluck('database')
                   ->flatMap(function ($database) {
                       return explode(',', $database);
                   })->filter()->unique()->values();
    }
}