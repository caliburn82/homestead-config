<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $fillable = ['name', 'from', 'to', 'type'];


    // ----------------------------------------------- relations

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

}
