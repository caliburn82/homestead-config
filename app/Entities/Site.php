<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'url', 'path', 'active', 'type', 'version', 'schedule', 'parameters', 'database', 'inherit_tld', 'group_id',
        'folder_id',
    ];

    protected $casts = [
        'active'      => 'boolean',
        'inherit_tld' => 'boolean',
        'schedule'    => 'boolean',
    ];


    // ----------------------------------------------- relations

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }


}
