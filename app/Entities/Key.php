<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    protected $fillable = ['location', 'active'];

    protected $casts = ['active' => 'boolean'];

}
