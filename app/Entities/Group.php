<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $fillable = ['name', 'tld'];


    // ----------------------------------------------- relations

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

}
