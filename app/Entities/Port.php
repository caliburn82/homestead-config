<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    protected $fillable = ['name', 'from', 'to', 'active', 'protocol'];

    protected $casts = ['active' => 'boolean'];
}
