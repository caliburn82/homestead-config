<?php

if (!function_exists('getallheaders')) {
    function getallheaders()
    {

        return collect($_SERVER)
            ->filter(function ($value, $key) {
                return starts_with($key, 'HTTP_');
            })
            ->mapWithKeys(function ($value, $key) {

                // convert from "HTTP_TEST_HEADER" to "Test-Header"

                $key = str_replace('_', ' ', substr($key, 5));
                $key = ucwords(strtolower($key));
                $key = str_replace(' ', '-', $key);

                return [$key => $value];

            });
    }
}
