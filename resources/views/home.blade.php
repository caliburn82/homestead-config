@extends('layouts.card')


@section('header')
    Current Config
    &nbsp; &nbsp; &nbsp;
    <a href="#" id="copy">Copy Config</a>
@endsection


@section('content')
    <pre id="config">{!! \App\ConfigHandler::render() !!}</pre>
@endsection


@section('scripts')
    <script>
        document.querySelector('#copy').addEventListener('click', e => {
            e.preventDefault();

            const el = document.createElement('textarea');
            el.value = document.querySelector('#config').innerText;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        });
    </script>
@endsection

