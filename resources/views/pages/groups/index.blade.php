@extends('layouts.card')


@section('header')
    Groups

    <a href="{{ route('groups.create') }}">Add new</a>
@endsection


@section('content')

    <dl>
        @foreach($groups as $group)
            <dt><a href="{{route('groups.edit', $group->id)}}">{{ $group->name }}</a></dt>
            <dd>{{ $group->tld ?? 'Not in use' }}</dd>
        @endforeach
    </dl>

@endsection
