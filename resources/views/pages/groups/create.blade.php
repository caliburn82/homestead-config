@extends('layouts.card')


@section('header', 'Default Settings')


@section('content')

    {!! Form::open(['route'=>['groups.store'], 'class' => 'groups']) !!}

    @include('pages.groups._form')

    {!! Form::close() !!}
@endsection
