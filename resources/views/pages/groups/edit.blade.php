@extends('layouts.card')


@section('content')

    {!! Form::model($group, ['route'=>['groups.update', $group], 'class' => 'groups']) !!}

    @include('pages.groups._form')
    <input type="submit" name="_method" value="Delete">


    {!! Form::close() !!}
@endsection
