@extends('layouts.card')


@section('header', 'Default Settings')


@section('content')

    {!! Form::open(['route'=>['folders.store'], 'class' => 'folders']) !!}

    @include('pages.folders._form')

    {!! Form::close() !!}
@endsection
