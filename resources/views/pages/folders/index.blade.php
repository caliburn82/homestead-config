@extends('layouts.card')


@section('header')
    Default Settings

    <a href="{{ route('folders.create') }}">Add new</a>
@endsection


@section('content')

    <dl>
        @foreach($folders as $folder)
            <dt><a href="{{route('folders.edit', $folder->id)}}">{{ $folder->name }}</a></dt>
            <dd>{{ "$folder->from => $folder->to" }}</dd>
        @endforeach
    </dl>

@endsection
