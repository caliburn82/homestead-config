@extends('layouts.card')


@section('content')

    {!! Form::model($folder, ['route'=>['folders.update', $folder], 'class' => 'folders']) !!}

    @include('pages.folders._form')
    <input type="submit" name="_method" value="Delete">


    {!! Form::close() !!}
@endsection
