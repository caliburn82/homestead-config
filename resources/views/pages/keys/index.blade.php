@extends('layouts.card')


@section('header')
    Default Settings

    <a href="{{ route('keys.create') }}">Add new</a>
@endsection


@section('content')

    <dl>
        @foreach($keys as $key)
            <dt><a href="{{route('keys.edit', $key->id)}}">{{ $key->location }}</a></dt>
            <dd>{{ $key->active ? 'active' : 'inactive' }}</dd>
        @endforeach
    </dl>

@endsection
