@extends('layouts.card')


@section('header', 'Default Settings')


@section('content')

    {!! Form::open(['route'=>['keys.store'], 'class' => 'keys']) !!}

    @include('pages.keys._form')

    {!! Form::close() !!}
@endsection
