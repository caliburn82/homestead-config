@extends('layouts.card')


@section('content')

    {!! Form::model($key, ['route'=>['keys.update', $key], 'class' => 'keys']) !!}

    @include('pages.keys._form')
    <input type="submit" name="_method" value="Delete">


    {!! Form::close() !!}
@endsection
