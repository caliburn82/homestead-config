@extends('layouts.card')


@section('header', 'Site Settings')


@section('content')

    {!! Form::open(['route'=>['sites.store'], 'class' => 'sites']) !!}

    @include('pages.sites._form')

    {!! Form::close() !!}
@endsection
