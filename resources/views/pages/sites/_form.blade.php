<div>
    {!! Form::label('folder_id', 'Folder:') !!}
    {!! Form::select('folder_id', $folders, null) !!}
</div>

<div>
    {!! Form::label('group_id', 'Group:') !!}
    {!! Form::select('group_id', $groups, null, ['placeholder' => 'None']) !!}
</div>

<div>
    {!! Form::label('url', 'URL:') !!}
    {!! Form::text('url') !!} <span id="tld">. {{ $site->group->tld ?? '' }} . {{ config('cm.tld') }}</span>
</div>

<div>
    {!! Form::hidden('inherit_tld', 0) !!}
    {!! Form::label('inherit_tld', 'Inherit TLD:') !!}
    {!! Form::checkbox('inherit_tld', 1, isset($site) ? null : true) !!}
</div>

<div>
    {!! Form::label('path', 'Path:') !!}
    {!! Form::text('path') !!}
</div>


<div>
    {!! Form::label('database', 'Database:') !!}
    {!! Form::text('database') !!}
</div>

<div>
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type') !!}
</div>

<div>
    {!! Form::label('version', 'Version:') !!}
    {!! Form::text('version') !!}
</div>

<div>
    {!! Form::hidden('active', 0) !!}
    {!! Form::label('active', 'Active:') !!}
    {!! Form::checkbox('active', 1, isset($site) ? null : true) !!}
</div>

<div>
    {!! Form::hidden('schedule', 0) !!}
    {!! Form::label('schedule', 'Schedule:') !!}
    {!! Form::checkbox('schedule', 1) !!}
</div>

<div>
    {!! Form::label('parameters', 'Parameters:') !!}
    {!! Form::text('parameters') !!}
</div>


<input type="submit" value="Save">
