@extends('layouts.card')


@section('content')

    {!! Form::model($site, ['route'=>['sites.update', $site], 'class' => 'sites']) !!}

    @include('pages.sites._form')
    <input type="submit" name="_method" value="Delete">

    <input type="submit" name="action" value="Duplicate">


    {!! Form::close() !!}
@endsection
