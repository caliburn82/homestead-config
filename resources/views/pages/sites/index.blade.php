@extends('layouts.card')


@section('header')
    Groups
    &nbsp; &nbsp; &nbsp;
    <a href="{{ route('sites.create') }}">Add new</a>
    &nbsp; &nbsp; &nbsp;
    <a href="javascript:document.getElementById('site-list').submit()">Update "active" sites</a>
@endsection


@section('content')

    {{ Form::open([ 'route' => 'sites.update-all', 'id' => 'site-list']) }}

    @if ($active)
        <h2>Active</h2>
        <dl>
            @foreach ($active as $group => $sites)
                <h3>{{ $group ?: 'Unnamed Group' }}</h3>

                @foreach ($sites as $site)

                    <dt>{{ Form::checkbox('active[]', $site->id, true) }} <a
                                href="{{route('sites.edit', $site->id)}}">{{ $site->url ?: 'none' }}</a></dt>
                    <dd>{{ $site->path }}</dd>

                @endforeach


            @endforeach
        </dl>

        <br><br><br>
    @endif



    @if ($inactive)
        <h2>Inactive</h2>
        <dl>
            @foreach ($inactive as $group => $sites)
                <h3>{{ $group ?: 'Unnamed Group' }}</h3>

                @foreach ($sites as $site)

                    <dt>{{ Form::checkbox('active[]', $site->id) }} <a
                                href="{{route('sites.edit', $site->id)}}">{{ $site->url ?: 'none' }}</a></dt>
                    <dd>{{ $site->path }}</dd>

                @endforeach


            @endforeach
        </dl>
    @endif

    {!! Form::close() !!}

@endsection
