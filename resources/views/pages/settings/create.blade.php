@extends('layouts.card')


@section('header', 'Default Settings')


@section('content')

    {!! Form::open(['route'=>['settings.store'], 'class' => 'settings']) !!}

    @include('pages.settings._form')

    {!! Form::close() !!}
@endsection
