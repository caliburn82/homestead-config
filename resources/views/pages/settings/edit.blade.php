@extends('layouts.card')


@section('content')

    {!! Form::model($setting, ['route'=>['settings.update', $setting], 'class' => 'settings']) !!}

    @include('pages.settings._form')
    <input type="submit" name="_method" value="Delete">


    {!! Form::close() !!}
@endsection
