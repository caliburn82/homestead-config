@extends('layouts.card')


@section('header')
    Default Settings

    <a href="{{ route('settings.create') }}">Add new</a>
@endsection


@section('content')

    <dl>
        @foreach($settings as $setting)
            <dt><a href="{{route('settings.edit', $setting->id)}}">{{$setting->key}}</a></dt>
            <dd>{{$setting->value}}</dd>
        @endforeach
    </dl>

@endsection
