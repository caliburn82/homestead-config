<ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            Settings <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('settings.index') }}">Defaults</a>
            <a class="dropdown-item" href="{{ route('keys.index') }}">Keys</a>
            <a class="dropdown-item" href="{{ route('folders.index') }}">Folders</a>
            <a class="dropdown-item" href="{{ route('groups.index') }}">Groups</a>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('sites.index') }}">Sites</a>
    </li>
</ul>
