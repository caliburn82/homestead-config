<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableSeeder::class);
        $this->call(KeysTableSeeder::class);
        $this->call(FoldersTableSeeder::class);
        $this->call(PortsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(SitesTableSeeder::class);
    }
}
