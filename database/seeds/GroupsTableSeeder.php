<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaults = [
            'created_at' => now(),
            'updated_at' => now(),
            'tld'        => null,
        ];

        DB::table('groups')->insert([
            [
                'name' => 'Personal'
            ] + $defaults,
            [
                'name' => 'Balance',
                'tld'  => 'bal'
            ] + $defaults,
            [
                'name' => 'By Brett',
                'tld'  => 'br'
            ] + $defaults,
            [
                'name' => 'Development',
                'tld'  => 'dev'
            ] + $defaults,
            [
                'name' => 'Dudley Building Society',
                'tld'  => 'dbs'
            ] + $defaults,
            [
                'name' => 'Generali',
                'tld'  => 'gw'
            ] + $defaults,
            [
                'name' => 'Gorey Wines',
                'tld'  => 'gry'
            ] + $defaults,
            [
                'name' => 'MMS',
                'tld'  => 'mms'
            ] + $defaults,
            [
                'name' => 'HSBC',
                'tld'  => 'hsbc'
            ] + $defaults,
            [
                'name' => 'Love Maths',
                'tld'  => 'lm'
            ] + $defaults,
            [
                'name' => 'Meridian Metals',
                'tld'  => 'mm'
            ] + $defaults,
            [
                'name' => 'officebroker',
                'tld'  => 'ob'
            ] + $defaults,
            [
                'name' => 'PC Paramedics',
                'tld'  => 'pcp'
            ] + $defaults,
            [
                'name' => 'Rubis',
                'tld'  => 'ru'
            ] + $defaults,
            [
                'name' => 'The Partnership',
                'tld'  => 'tp'
            ] + $defaults,
            [
                'name' => 'Unipart Group',
                'tld'  => 'uep'
            ] + $defaults,
        ]);
    }
}
