<?php

use Illuminate\Database\Seeder;

class FoldersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('folders')->insert([
            'created_at' => now(),
            'updated_at' => now(),
            'name'       => 'default projects',
            'from'       => 'D:\projects',
            'to'         => '/var/www',
        ]);
    }
}
