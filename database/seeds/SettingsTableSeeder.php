<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaults = [
            'created_at' => now(),
            'updated_at' => now(),
        ];

        DB::table('settings')->insert([
            ['key' => 'ip', 'value' => '192.168.10.10'] + $defaults,
            ['key' => 'memory', 'value' => 2048] + $defaults,
            ['key' => 'cpus', 'value' => 1] + $defaults,
            ['key' => 'provider', 'value' => 'virtualbox'] + $defaults,
            ['key' => 'authorize', 'value' => '~/.ssh/id_rsa.pub'] + $defaults,
        ]);
    }
}
