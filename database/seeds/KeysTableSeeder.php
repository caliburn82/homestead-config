<?php

use Illuminate\Database\Seeder;

class KeysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keys')->insert([
            'location'   => '~/.ssh/id_rsa',
            'active'     => true,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
