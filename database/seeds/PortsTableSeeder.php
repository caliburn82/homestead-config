<?php

use Illuminate\Database\Seeder;

class PortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $defaults = [
            'created_at' => now(),
            'updated_at' => now(),
            'active'     => true,
            'protocol'   => null,
        ];

        DB::table('ports')->insert([
            ['name' => 'http', 'from' => 80, 'to' => 80] + $defaults,
            ['name' => 'https', 'from' => 443, 'to' => 443] + $defaults,
            ['name' => 'mysql', 'from' => 3306, 'to' => 3306] + $defaults,
            ['name' => 'postgresql', 'from' => 5432, 'to' => 5432] + $defaults,
            ['name' => 'ssh', 'from' => 22, 'to' => 22] + $defaults,
        ]);
    }
}
