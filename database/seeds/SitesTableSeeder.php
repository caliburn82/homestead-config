<?php

use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $groups  = DB::table('groups')->pluck('id', 'name');
        $folders = DB::table('folders')->pluck('id', 'name');

        $defaults = [
            'active'      => false,
            'path'        => null,
            'type'        => null,
            'version'     => null,
            'schedule'    => false,
            'parameters'  => null,
            'database'    => null,
            'created_at'  => now(),
            'updated_at'  => now(),
            'inherit_tld' => true,
            'group_id'    => null,
            'folder_id'   => $folders->get('default projects')
        ];

        DB::table('sites')->insert([
            [
                'url'         => '192.168.10.10',
                'active'      => true,
                'inherit_tld' => false,
            ] + $defaults,


            [
                'url'      => 'config-manager',
                'path'     => '_dev/config manager/public',
                'active'   => true,
                'group_id' => $groups->get('Personal'),
                'database' => 'homestead'
            ] + $defaults,
            [
                'url'      => 'comic-reader',
                'path'     => '_personal/web-reader',
                'active'   => true,
                'group_id' => $groups->get('Personal'),
            ] + $defaults,
            [
                'url'      => 'file-renamer',
                'path'     => '_personal/file-renamer/public',
                'group_id' => $groups->get('Personal'),
            ] + $defaults,


            [
                'url'      => '',
                'path'     => '_dev',
                'active'   => true,
                'group_id' => $groups->get('Development'),
            ] + $defaults,
            [
                'url'      => 'auto-deployment',
                'path'     => '_dev/auto-deployment/public',
                'group_id' => $groups->get('Development'),
            ] + $defaults,
            [
                'url'      => 'suite-crm',
                'path'     => 'dev-suite_crm/public',
                'group_id' => $groups->get('Development'),
            ] + $defaults,
            [
                'url'      => 'magento',
                'path'     => 'dev-magento',
                'group_id' => $groups->get('Development'),
            ] + $defaults,
            [
                'url'      => 'api.oauth',
                'path'     => '_dev/oauth/api/public',
                'active'   => true,
                'group_id' => $groups->get('Development'),
            ] + $defaults,
            [
                'url'      => 'oauth',
                'path'     => '_dev/oauth/client/public',
                'active'   => true,
                'group_id' => $groups->get('Development'),
            ] + $defaults,


            [
                'url'      => 'balance',
                'path'     => 'balance/dist',
                'group_id' => $groups->get('Balance'),
            ] + $defaults,


            [
                'url'      => 'by-brett',
                'path'     => 'br-by_brett/web',
                'group_id' => $groups->get('By Brett'),
                'active'   => true,
            ] + $defaults,


            [
                'url'      => 'pattern-library',
                'path'     => 'dbs-pattern-library/public/public',
                'group_id' => $groups->get('Dudley Building Society'),
            ] + $defaults,
            [
                'url'      => 'print-on-demand',
                'path'     => 'dbs-print_on_demand/public',
                'group_id' => $groups->get('Dudley Building Society'),
                'version'  => 5.6,
                'active'   => false,
                'database' => 'dbs-print_on_demand',
            ] + $defaults,
            [
                'url'         => 'savewithdbs.co.uk',
                'path'        => 'dbs-print_on_demand/public',
                'group_id'    => $groups->get('Dudley Building Society'),
                'version'     => 5.6,
                'inherit_tld' => false,
            ] + $defaults,


            [
                'url'         => 'dmp',
                'path'        => 'gw-dms/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Generali'),
            ] + $defaults,
            [
                'url'         => 'journey',
                'path'        => 'gw-journey/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Generali'),
            ] + $defaults,
            [
                'url'         => 'pension-transfers-hub',
                'path'        => 'gw-pension_transfers/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Generali'),
            ] + $defaults,
            [
                'url'         => 'techlife',
                'path'        => 'gw-techlife/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Generali'),
            ] + $defaults,
            [
                'url'      => 'welcome-pack',
                'path'     => 'gw-welcome-pack/public',
                'group_id' => $groups->get('Generali'),
            ] + $defaults,


            [
                'url'      => 'wine-cellar',
                'path'     => 'gry-wine_cellar',
                'group_id' => $groups->get('Gorey Wines'),
                'database' => 'gry-wine_cellar',
                'type'     => 'nested-wordpress',
            ] + $defaults,

            [
                'url'      => 'wordpress.wine-cellar',
                'path'     => 'gry-wine_cellar/wordpress',
                'group_id' => $groups->get('Gorey Wines'),
            ] + $defaults,


            [
                'url'      => 'connections',
                'path'     => 'mms-connections/public',
                'group_id' => $groups->get('MMS'),
            ] + $defaults,
            [
                'url'      => 'contact-builder',
                'path'     => 'mms-contact-builder/public',
                'group_id' => $groups->get('MMS'),
            ] + $defaults,


            [
                'url'      => 'banner-ads',
                'path'     => 'hsbc-banner_ad_generator/public',
                'group_id' => $groups->get('HSBC'),

            ] + $defaults,


            [
                'url'      => 'resources',
                'path'     => 'lm-resources/public',
                'group_id' => $groups->get('Love Maths'),
                'version'  => 5.6,

            ] + $defaults,
            [
                'url'      => 'wordpress',
                'path'     => 'lm-wordpress/public',
                'group_id' => $groups->get('Love Maths'),
                'version'  => 5.6,

            ] + $defaults,


            [
                'url'      => 'reporting',
                'path'     => 'mm-reporting/public',
                'group_id' => $groups->get('Meridian Metals'),
            ] + $defaults,


            [
                'url'      => 'affiliate-login',
                'path'     => 'ob-affiliate_login/web',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
                'type'     => 'symfony2',
            ] + $defaults,
            [
                'url'      => 'londonoffices',
                'path'     => 'ob-londonoffices/web',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'zoneonecommercial',
                'path'     => 'ob-zoneone_commercial/public',
                'group_id' => $groups->get('officebroker'),
            ] + $defaults,
            [
                'url'      => 'images',
                'path'     => 'ob-xml/web/images',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
                'type'     => 'symfony2',
            ] + $defaults,
            [
                'url'         => 'images.officebroker.com',
                'path'        => 'ob-xml/web/images',
                'inherit_tld' => false,
                'group_id'    => $groups->get('officebroker'),
                'version'     => 5.6,
                'type'        => 'symfony2',
            ] + $defaults,
            [
                'url'      => 'search-report',
                'path'     => 'ob-search_report/web',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
                'type'     => 'symfony2',
            ] + $defaults,
            [
                'url'      => 'starofficespace',
                'path'     => 'ob-officialspace/public',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'officialspace',
                'path'     => 'ob-officialspace/public',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
                'active'   => true,
                'type'     => 'nested-wordpress',
            ] + $defaults,
            [
                'url'      => 'www',
                'path'     => 'ob-www/public',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'xml',
                'path'     => 'ob-xml/web',
                'group_id' => $groups->get('officebroker'),
                'version'  => 5.6,
                'active'   => true,
                'type'     => 'symfony2',
            ] + $defaults,
            [
                'url'         => 'xml.officebroker.com',
                'path'        => 'ob-xml/web',
                'inherit_tld' => false,
                'group_id'    => $groups->get('officebroker'),
                'version'     => 5.6,
                'type'        => 'symfony2',
            ] + $defaults,


            [
                'url'      => 'image-library.pcp',
                'path'     => 'pcp-imagelibrary',
                'group_id' => $groups->get('PC Paramedics'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'wordpress.pcp',
                'path'     => 'pcp-wordpress-build/public',
                'group_id' => $groups->get('PC Paramedics'),
                'version'  => 5.6,
            ] + $defaults,


            [
                'url'      => 'lacollette',
                'path'     => 'ru-lacollette/public',
                'group_id' => $groups->get('Rubis'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'         => 'lacolletteterminal.co.uk',
                'path'        => 'ru-lacollette/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Rubis'),
                'version'     => 5.6,
            ] + $defaults,
            [
                'url'         => 'www.lacolletteterminal.co.uk',
                'path'        => 'ru-lacollette/public',
                'inherit_tld' => false,
                'group_id'    => $groups->get('Rubis'),
                'version'     => 5.6,
            ] + $defaults,
            [
                'url'      => 'wordpress',
                'path'     => 'ru-wordpress_site/public',
                'group_id' => $groups->get('Rubis'),
            ] + $defaults,


            [
                'url'      => 'audit-generator',
                'path'     => 'tp-audit_generator/public',
                'group_id' => $groups->get('The Partnership'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'numbers',
                'path'     => '_old/lara-test-jon/public',
                'group_id' => $groups->get('The Partnership'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'product-launcher',
                'path'     => 'tp-product_launcher/public',
                'group_id' => $groups->get('The Partnership'),
                'version'  => 7.1,
            ] + $defaults,
            [
                'url'      => 'voting',
                'path'     => 'tp-voting-system/public',
                'group_id' => $groups->get('The Partnership'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'wordpress',
                'path'     => 'tp-wordpress/public',
                'group_id' => $groups->get('The Partnership'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'cst',
                'path'     => 'tp-cst_portal/public',
                'group_id' => $groups->get('The Partnership')
            ] + $defaults,


            [
                'url'      => 'audit-generator',
                'path'     => 'uep-audit_generator-55/public',
                'group_id' => $groups->get('Unipart Group'),
            ] + $defaults,
            [
                'url'      => 'old.audit-generator',
                'path'     => 'uep-audit_generator-51/public',
                'group_id' => $groups->get('Unipart Group'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'digital-harbour',
                'path'     => 'uep-digital_harbour/public',
                'group_id' => $groups->get('Unipart Group'),
                'version'  => 7.1,
            ] + $defaults,
            [
                'url'      => 'snapp',
                'path'     => 'uep-sales_process/public',
                'group_id' => $groups->get('Unipart Group'),
                'version'  => 5.6,
            ] + $defaults,
            [
                'url'      => 'suite-crm',
                'path'     => 'uep-suite_crm',
                'group_id' => $groups->get('Unipart Group'),
            ] + $defaults,
        ]);


    }
}
