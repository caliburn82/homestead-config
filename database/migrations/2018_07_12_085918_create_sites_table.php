<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('url');
            $table->string('path')->nullable();
            $table->boolean('active')->default(true);
            $table->string('type')->nullable();
            $table->string('version')->nullable();
            $table->boolean('schedule')->default(false);
            $table->json('parameters')->nullable();

            $table->string('database')->nullable();
            $table->boolean('inherit_tld')->default(true);

            $table->unsignedInteger('group_id')->nullable()->index();
            $table->foreign('group_id')
                  ->references('id')->on('groups')
                  ->onDelete('set null');

            $table->unique(['url', 'group_id']);

            $table->unsignedInteger('folder_id')->nullable()->index();
            $table->foreign('folder_id')->references('id')->on('folders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
