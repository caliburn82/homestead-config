<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

    Route::get('settings/{setting?}', 'SettingsController@get')->name('settings.get');
    Route::put('settings/{setting?}', 'SettingsController@put')->name('settings.put');
    Route::patch('settings/{setting}', 'SettingsController@patch')->name('settings.patch');
    Route::delete('settings/{setting}', 'SettingsController@delete')->name('settings.delete');

    Route::get('keys/{key?}', 'KeysController@get')->name('keys.get');
    Route::put('keys/{key?}', 'KeysController@put')->name('keys.put');
    Route::patch('keys/{key}', 'KeysController@patch')->name('keys.patch');
    Route::delete('keys/{key}', 'KeysController@delete')->name('keys.delete');

});

