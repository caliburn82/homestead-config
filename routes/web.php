<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'home')->name('home');

Route::get('active.rss', 'FeedController@active')->name('rss.active');

Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
    Route::get('/', 'SettingsController@index')->name('index');
    Route::get('/create', 'SettingsController@create')->name('create');
    Route::post('/create', 'SettingsController@store')->name('store');
    Route::get('/{setting}', 'SettingsController@edit')->name('edit');
    Route::post('/{setting}', 'SettingsController@update')->name('update');
    Route::delete('/{setting}', 'SettingsController@delete')->name('delete');
});


Route::group(['prefix' => 'keys', 'as' => 'keys.'], function () {
    Route::get('/', 'KeysController@index')->name('index');
    Route::get('/create', 'KeysController@create')->name('create');
    Route::post('/create', 'KeysController@store')->name('store');
    Route::get('/{key}', 'KeysController@edit')->name('edit');
    Route::post('/{key}', 'KeysController@update')->name('update');
    Route::delete('/{key}', 'KeysController@delete')->name('delete');
});

Route::group(['prefix' => 'folders', 'as' => 'folders.'], function () {
    Route::get('/', 'FoldersController@index')->name('index');
    Route::get('/create', 'FoldersController@create')->name('create');
    Route::post('/create', 'FoldersController@store')->name('store');
    Route::get('/{folder}', 'FoldersController@edit')->name('edit');
    Route::post('/{folder}', 'FoldersController@update')->name('update');
    Route::delete('/{folder}', 'FoldersController@delete')->name('delete');
});

Route::group(['prefix' => 'groups', 'as' => 'groups.'], function () {
    Route::get('/', 'GroupsController@index')->name('index');
    Route::get('/create', 'GroupsController@create')->name('create');
    Route::post('/create', 'GroupsController@store')->name('store');
    Route::get('/{group}', 'GroupsController@edit')->name('edit');
    Route::post('/{group}', 'GroupsController@update')->name('update');
    Route::delete('/{group}', 'GroupsController@delete')->name('delete');
});

Route::group(['prefix' => 'sites', 'as' => 'sites.'], function () {
    Route::get('/', 'SitesController@index')->name('index');
    Route::post('/', 'SitesController@updateAll')->name('update-all');
    Route::get('/create', 'SitesController@create')->name('create');
    Route::post('/create', 'SitesController@store')->name('store');
    Route::get('/{site}', 'SitesController@edit')->name('edit');
    Route::post('/{site}', 'SitesController@update')->name('update');
    Route::delete('/{site}', 'SitesController@delete')->name('delete');
});
