# What?

This is just a basic homestead config manager I made to make my life easier.

## Why?
I had a lot of projects constantly commented out all the time and got bored of trying to manage the various settings,
etc manually.

Feel free to nick and use this, but if it breaks anything, don't blame me.

## Use

The config file 'cm.php' had the TLD stored in is as .test - change it to anything you like. API pagination is in there
too - not really in use at present.

Make your changes, experiment, and otherwise work it out!

Copy the end config over your homestead.yml and re-provision and run host manager.


# API

I started to make this to mess with basic REST API handling, (GET, POST, PATCH, PUT, DELETE), so some of this it up and
running. Again, feel free to expand on it if you like.

# Future

This should probably be made into a mini-desktop app to handle homestead and store over the config file, restart
homestead, handle backups, etc. I'm lazy though, so I wouldn't hold my breath (*_o)
